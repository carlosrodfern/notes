import matplotlib.pyplot as plt
import numpy as np

def usl(N1, N, alpha, beta):
    return N1 * N/(1 + alpha * (N - 1) + beta * N * (N - 1))

filename = "data.txt"		# data file

data_xs=[]
data_ys=[]

N1 = None # first point in the data file
with open(filename, 'r') as f:
    f.readline()
    for line in f:
        values = [float(i.strip()) for i in line.split('\t')]
        if N1 is None:
            N1 = values[1]
        data_xs.append(values[0])
        data_ys.append(values[1])

xs = np.arange(1, 21, 1)  # Sample data.

fig, (ax, alphax, betax) = plt.subplots(1,3)
alpha = 0.0326
beta = 0.00244
ax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
ax.plot(data_xs, data_ys, 'ro', label='observed')
ax.set_xticks(xs)
ax.set_xlabel('CPUs (N)')  # Add an x-label to the axes.
ax.set_ylabel('Throughput')  # Add a y-label to the axes.
ax.set_title('Universal Scalability Law model')  # Add a title to the axes.
ax.legend()  # Add a legend.

alpha = 0.015
beta = 0.0028
alphax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
alpha = 0.025
beta = 0.0028
alphax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
alpha = 0.035
beta = 0.0028
alphax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
alphax.set_title('Incrementing alpha')
alphax.legend()

alpha = 0.025
beta = 0.0018
betax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
alpha = 0.025
beta = 0.0028
betax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
alpha = 0.025
beta = 0.0038
betax.plot(xs, [usl(N1,x,alpha,beta) for x in xs], label=f'alpha={alpha}, beta={beta}')
betax.set_title('Incrementing beta')
betax.legend()

plt.show()