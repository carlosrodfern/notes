import ciw
import matplotlib.pyplot as plt 

# To define an M/M/3 queue, with λ = 20 and μ = 10
N = ciw.create_network(
    arrival_distributions=[ciw.dists.Exponential(rate=30)], # arrival rate, e.g. 20 req/sec
    service_distributions=[ciw.dists.Exponential(rate=10)], # service rate per server, e.g. 10 req/sec mean
    number_of_servers=[3]
)
ciw.seed(1)
Q = ciw.Simulation(N)
Q.simulate_until_max_time(1440)

recs = Q.get_all_records()
print([n.server_utilisation for n in Q.transitive_nodes])
waits = [r.waiting_time for r in recs]
plt.hist(waits)
plt.show()
