import matplotlib.pyplot as plt
import numpy as np


def fac(x):
    if x == 0:
        return 1.0
    else:
        return x * fac(x-1)

def part1(m,p):
    return sum([((m*p)**k)/fac(k) for k in range(1, m)])

def part2(m,p):
    return ((m*p)**m)/fac(m)

def part3(p):
    return 1/(1-p)

def p0(m,p):
    return (part1(m,p) + part2(m,p)*part3(p))**-1

def nc(m,p):
    return m*p + p*((m*p)**m)*p0(m,p)/(fac(m)*((1-p)**2))

def t(m,p,u):
    return nc(int(m),p)/(u*p*m)

# response time by utilization for capacity of 100 req/s for 1 servers and 10 servers

fig, ax = plt.subplots()

xs = np.arange(0.01, 1.0, 0.01)

ax.plot(xs, [t(1,x,100) for x in xs], label='m=1')
ax.plot(xs, [t(3,x,100) for x in xs], label='m=3')
ax.plot(xs, [t(10,x,100) for x in xs], label='m=10')
ax.set_xlabel('Utilization')  # Add an x-label to the axes.
ax.set_ylabel('Latency (s)')  # Add a y-label to the axes.
ax.set_title('Latency by utilization in M/M/m queue. 100 req/s.')  # Add a title to the axes.
ax.legend()
plt.show()