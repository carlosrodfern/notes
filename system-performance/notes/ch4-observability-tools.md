# Utilization and Saturation of CPU

* per-CPU utilization: `mpstat -P ALL 1`
* per-process CPU utilization: `top`, `pipstat 1`
* per-thread run queue (scheduler) latency: `/proc/PID/schedstats`
* CPU run queue latency: `/proc/schedstat`, `runqlat`
* CPU run queue length: `vmstat 1` (r column), `runqlen`

# Network Performance

```sh
# Micro-benchmarking

# on one node, let's say 10.0.0.123, to start a server
iperf -s -p 5001

# on the other node
# against the 10.0.0.123 node, for 10 sec, interval 1sec.
iperf -c 10.0.0.123 -t 10 -i 1

```

# Profiling

```
# record all activity for 10 seconds, at 99Hz frequence, -a all cpus, -g all call-graph
sudo perf record -F 99 -a -g -- sleep 10
# generate flamegraph
perf script report flamegraph
```


```
# record program until it exists, at 99Hz frequence, -a all cpus, -g all call-graph
sudo perf record -F 99 -g -a <command>
# generate flamegraph
perf script report flamegraph
```

# Static Analysis

```sh
# find the classes and devices
lshw -short
# list details of a class
lshw -class network
```

```sh
df -h
```

```sh
nmcli device status
nmcli conn show
nmcli device wifi
```

# Logs Analysis

```sh
# error(3), critical (2), alert (1) and emergency (0) errors since last boot.
journalctl -b -1 --priority=3
```

# Saturation with PSI

```console
# cat /proc/pressure/cpu
some avg10=0.00 avg60=0.00 avg300=0.00 total=78239379
full avg10=0.00 avg60=0.00 avg300=0.00 total=70869262

# cat /proc/pressure/io
some avg10=0.00 avg60=0.00 avg300=0.00 total=68701836
full avg10=0.00 avg60=0.00 avg300=0.00 total=67806896

# cat /proc/pressure/memory
some avg10=0.00 avg60=0.00 avg300=0.00 total=1068989
full avg10=0.00 avg60=0.00 avg300=0.00 total=1024216
```

% of time stalled on a resource. `some` is a thread has stalled % of time. `full` is all non-idle threads have stalled % of time.
Avg available for 10sec, 60sec, and 300sec, to see trend upward or downward.

It answers the question: who likely is it that a task will have to wait on the resources?

# System Activity with `sar`

`sar -s [HH:mm:ss] -e [HH:mm:ss] [options] [-days]`: activity during start (`-s`) and end (`-e`), today or `days` ago (e.g. `-1` for yesterday and so on)

Enable collection:

```
systemctl start sysstat sysstat-collect.timer sysstat-summary.timer
systemctl enable sysstat sysstat-collect.timer sysstat-summary.timer
```

# `bpftrace`

Get vmlinux headers:
`bpftool btf dump file /sys/kernel/btf/vmlinux format c > vmlinux.h`

Can use `kfunc:vmlinux:*` and `kretfunc:vmlinux:*` to use those types without casting or importing headers.
