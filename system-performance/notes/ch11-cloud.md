# Observability

## From the guest

```sh
# the st "stolen cpu time" column
vmstat -w 1
```


## Containers

Statistics are available at `/sys/fs/cgroups`

```sh
cd /sys/fs/cgroup/cpu,cpuacct/docker/48329a9b993f...
cat cpuacct.usage
# 12637162371
cat cpu.stat
# nr_periods 507
# nr_throttled 74
# throttled_time 5834784738
```

`nr_throttled` shows if it has been cpu throttled.

```sh
# statistics are also available via:
systemd-cgtop

# and:
htop
```

Find pid of container in k8s:
```sh
# get docker container id and host
kubectl describe pod [pod]

# inside host
ps -e -o pid,cgname,cmd | grep [container id]

# identify the process(es) belonging to the container that you want to look at, and grab their pid
```


```sh
# /proc/[pid]/status shows the NS pid (host pid and container pid)
cat /proc/4915/status
# ...
# NSpid:   4915   753
# ...

# reverse the search (given a container pid, find host pid)
awk '$1 == "NSpid:" && $3 === 753 { print $2 }' /proc/*/status
```

```sh
# /proc/[pid]/ns/pid, the host and the container one, both point to the ns id.
# in container
ls -lh /proc/753/ns/pid
lrwxrwxrwx 1 root root 0 Jan 1 00:00 /proc/753/ns/pid -> 'pid:[12345678]'

# in host
ls -lh /proc/4915/ns/pid
lrwxrwxrwx 1 root root 0 Jan 1 00:00 /proc/4915/ns/pid -> 'pid:[12345678]'
```

```sh
# container root / on /proc/[host pid]/root/
ls -lh /proc/4915/root/tmp
total 0
-rw-r--r-- 1 root root 0 Jan 1 00:00 from-the-container.txt
```


```sh
# runs commands in the selected namespace
# -m in the mount
# -p in the process namespace
# -t from host pid
nsenter -t [host pid] -m -p top
```

```
// cgroup var is available in bpftrace to trace by cgroup
// curtask contains also info about the node

#include <linux/sched.h>
#include <linux/nsproxy.h>
#include <linux/utsname.h>

t:syscalls:sys_enter_fork,
t:syscalls:sys_enter_vfork,
t:syscalls:sys_enter_clone
{
    $task = (struct task_struct *) curtask;
    $nodename = $task->nsproxy->uts_ns->name.nodename;
    @new_processes[$nodename] = count();
}
```

Find out container throttling:

```
if Throttled time increasing then Bandwidth throttled. Done.
if Non-voluntary context switches increasing then
  if Host has idle CPUs then
    if cpu set CPUs 100% busy then
      cpuset throttled
    else
      Not throttled
  else
    if All other tenants idle then
      Physical CPU throttled
    else
      Shared throttled
else
  Not throttled
```

* **Throttled time**: cpu cgroup throttled_time
* **Non-voluntary context switches**: `/proc/[pid]/status` the `nonvoluntary_ctxt_switches`, `pidstat -w 1`.
* **Host has idle CPU**: `%idle` in `mpstat(1)`
* **cpuset CPUs 100% busy**: `mpstat(1)` and `/proc/stat`
* **All other tenants idle**: `docker stat`, and other container specific tools


Make `bpftrace` aware of the container mount namespace so it can resolve the symbols:

```sh
bpftrace -p [host pid of process in container] -e '...'
```

Make `perf` aware of the container mount namespace so it can resolve the symbols:

```sh
# It is working automatically on modern versions of perf
```

# Libvirt Snippets

## Install

```sh
cp ./QCOW2-orig/CentOS-Stream-GenericCloud-9-latest.x86_64.qcow2 ./QCOW2/cs9-test-1.qcow2
virt-install \
  --name cs9-test-1 \
  --memory 2048 \
  --vcpus 2 \
  --disk ./QCOW2/cs9-test-1.qcow2 \
  --import \
  --os-variant centos-stream9 \
  --graphics none \
  --network bridge:virbr0
```

## Setup `centos` sudo user

Assuming the machine is called cs9-test-1, and shutdown:
```sh
virt-sysprep -d cs9-test-1 --run-command "useradd centos; usermod -aG wheel centos; echo -e 'centos\tALL=(ALL)\tNOPASSWD: ALL' >> /etc/sudoers" --ssh-inject centos:file:/home/carlos/.ssh/id_rsa.pub
```

## Expand drive

```sh
qemu-img resize cs9-test-1.qcow2 +10G
```

Then use `growpart` and `xfs_growfs` to expand the file system.

```sh
growpart /dev/vda 1
xfs_growfs /
```

## Find IP

Assuming is running
```sh
virsh domiflist cs9-test-1
virsh net-dhcp-leases default
```

## Delete the machine

```sh
virsh undefine cs9-test-1 --remove-all-storage
```
