# Quick perf analysis in 1 minute

```sh

# CPU
# queue and PSI if available
sar -q ALL 1
# utilization
mpstat -u -P ALL 1

# MEM
# kernel errors including OOM events
dmesg -T | tail
# swap
free -hw
# swapping
sar -B 1

# Disk IO
iostat -sxz 1

# Network
# network device i/o: packets and throughput
sar -n DEV 1
# tcp stats: conection rates, retransmits
sar -n TCP,ETCP 1

# check overview
atop

# Quick drill down to process
# cpu utilization
pidstat -u -p [pid,] 1
# nv context switching
pidstat -w -p [pid,] 1
# disk utilization
pidstat -d -p [pid,] 1
# memory utilization
pidstat -r -p [pid,] 1
# network
ss -itopn
```

# Scientific Method

1. Question
2. Hypothesis
3. Prediction
4. Test
5. Analysis

Example (Observational):
1. What is causing slow database queries
2. Noisy neighbors are performing disk i/o, contending with database disk i/o
3. If file system i/o latency is measuerd during a query, it will show that the file system is responsible for the slow queries
4. Tracing teh database file system latency as a ratio of query latency shows that less than 5% of the time is spent waiting for the fs.
5. The file system and disks are not responsible for slow queries

Example (Experimental)
1. Why did fs performance degrade as the fs cache grew in size?
2. A larger cache stores more records, and more compute is required to manage a larger cache
3. Making the record size progressively smaller, and therefore causing more records to be used to store the same amount of data will make performance progressively worse.
4. Test the same workload with progressively smaller record sizes.
5. Results are graphed and are consistent with the predictions. Drill-down analysis is now performed on the cache management routines

# The USE Method

For every resource, check utilization, saturation, and errors. It is based on queuing theory: utilization of servers, saturation of the queues, errors as result of dropped calls.

* Resources: cpu, buses, etc...
* Utilization: for a set time interval, the % of time that the resource is busy. The resource may be busy but still able to serve requests. The degree to which it cannot do so is identified by saturation
* Saturation: the degree to which a resource has extra work that it can't service.
* Errors: count of error events


Procedure:

1. Identify resources
2. Choose a resourse
3. Check errors, if found investigate
4. Check saturation, if bad investigate
5. Check utilization, if bad investigate
6. Go to step 2.

Hardware Resouce List:

* CPU: utilization (per cpu, system-wide), saturation (run queue length, scheduler latency)
* Main Memory: utilization (available free memory), saturation (swapping, page scanning, OOM events)
* Network interfaces: utilization (recv/send throughput/maxbandwidth)
* Storage devices: utilization (dev busy %), saturation (wait queue length), errors (device errors, soft and hard)
* Accelerators:
* Controllers:
* Interconnect:

Software Resource List:

* Mutex locks
* Thread pools
* Process/thread capacity
* File descriptor capacity

Microservices:
* Utilization: the average cpu utilization across the cluster
* Saturation: an approx is the difference between average latency and 99th latency
* Errors: request errors

# The RED Method

* Request rate
* Errors
* Duration


# Workload Characterization

Find out what is causing the load.

# Drill-Down Analysis

Five whys with monitoring, identification, and analysis.

# Latency Analysis

Analyze the different contributors to a latency.

# Event Tracing

Information to look for in events:

* Input: all attributes of an event request
* Times: start time, end time, latency
* Result: error status, result of event

# Static Performance Tuning

System configuration.

Example:

* Network interface negotation: 100Mbps, 1 Gbps, 10 Gbps
* Failed disk in a RAID pool
* Older versions of OS, apps, fireware
* FS nearly full
* Mismatched fs record size compare with workload i/o size
* App running with debug mode
* Server configured as a network router (ip forwarding enabled)
* Server configured to use resources, such as auth, from a remote data center

## Micro-Benchmarking

Tests performance of simple and artificial workloads.

## Performance Mantras

1. Don't do it: eliminate unnecessary work
2. Do it, but don't do it again: caching
3. Do it less: tune refreshes, polling, or updates to be less frequent
4. Do it later: write-back caching
5. Do it when they're not looking: schedule work to run during off-peak hours
6. Do it concurrently: Switch from single-threaded to multi-threaded
7. Do it more cheaply: buy faster hardware.
