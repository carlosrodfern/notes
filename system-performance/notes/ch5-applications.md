# Methodologies

## Objectives

* Latency
* Throughput
* Efficiency: resource utilization
* Price: performance/price ratio

## Performance Techniques

* Selecting an I/O Size: larger i/o sizes usually brings better throughput, smaller, better latency (because of not wasted block sizes for smaller i/o).
* Caching
* Buffering for streams
* Polling mechanism (checks vs event-based)
* Concurrency and Parallelism
* Non-blocking i/o to reduce new thread overheads and context switching
* Processor binding: useful when NUMA because of memory locality, but can be a performance issue by hurting parallelism when there are other tenants as they assume they are the only processes in the system.

## Performance Mantras

* Don't do it
* Do it, but don't do it again
* Do it less
* Do it later
* Do it when they are not looking
* Do it concurrently
* Do it cheaper

## CPU Profiling

* `perf`

Typical off-CPU footprints in functions name:
* `ext4_`, or others: file system ops
* `blk_`: block i/o
* `tcp_`: network i/o
* `utex_`: locks mutex or futex
* `alloc_` or `object_`: code paths doing memory allocation

```sh
perf stat
```

## Off-CPU Analysis

* `offcputime`

## Syscall Analysis

* *New process tracing*: trace `execve`. Use `execsnoop`,
* *I/O profiling*: trace `read`, `write`, `send`, `recv`. See i/io sizes, flags, code paths, to find suboptimal i/o. Use `bpftrace`.
* *Kernel time analysis*: observe `%sys`. Also use `syscount`.

## USE Method

Example, application using pool of threads to handle requests:

* *Utilization*: average number of threads busy as % of total threads
* *Saturation*: average length of the request queue.
* *Errors*: requests denied or failed

Example, file descriptors:

* *Utilization*: number of in-use fd, as % of limit
* *Saturation*: number of blocked threads waiting for fd being released
* *Errors*: EFILE, "too many open files"


## Thread State Analysis

Where threads are spending their time.

Thread states:

* `Running` (Kernel time, user time), it is `TASK_RUNNING`.
* `Runnable`, it is `TASK_RUNNING`.
* `Sleep`, it is `TASK_INTERRUPTIBLE`.
* `Disk`, it is `TASK_UNINTERRUPTIBLE`.

Types of locks:

* Mutex locks: the holder of the lock is running, the rest is off-CPU
* Spin locks: the holder of the lock is running, while the rest _spin_ on-CPU in a tight loop. It provides lower latency but higher CPU utilization.
* RW locks: Reader/writer locks to ensure integrity of a memory block.
* Semaphores: to limit the number of running threads, while the rest is off-CPU.

In linux mutex locks implementations:

* `fastpath`: attempts to acquire the lock with `cmpxchg`, and it fails or succeeds immediately.
* `midpath`: the spin lock.
* `slowpath`: this blocks and deschedules the task.

Clue-based State Analysis:

* *User*: `pidstat` `%usr`
* *Kernel*: `pidstat` `%sys`
* *Runnable*: `vmstat`, `r` column (system-wide)
* *Swapping*: `vmstat`, `si` and `so` column (system-wide)
* *Disk I/O*: `pidstat -d`, `iodelay` (includes swapping state)
* *Network I/O*: `sar -n DEV` (system-wide)
* *Lock*: `perf top`

Direct Measurement State Analysis:

* *User*: `pidstat` `%usr`, `/proc/PID/stat`
* *Kernel*: `pidstat` `%sys`, `/proc/PID/stat`
* *Runnable*: `/proc/PID/schedstat`, `runqlat`, `perf sched`.
* *Swapping*: anonymous paging, with delay accounting
* *Disk I/O*: `pidstat -d`, `iodelay` (includes swapping state), `biotop`, `iotop`.
* *Network I/O*: `bpftrace`, `tcptop`.
* *Lock*: `klockstat`


## Lock Analysis

* Checking for contention
* Checking for excessive hold times

CPU profiling could do it for _spin locks_ and for _adaptive mutex locks_. `bpftrace` can help to.

# Observability Tools

BPF tools collect the events, sends them to the kernel ring buffer, and reads them into the user-space. It is a lot more efficient for tracing than other technologies.

## `perf` CPU profiling, CPU flame graphs, syscall tracing

```sh

# system-wide 30 sec
perf record -F 49 -a -g -- sleep 30

# process
perf record -F 99 -g -p 1234

# flame graphs
perf script report flamegraph
# better alternative
perf script --header > out.stacks
stackcollapse-perf.pl < out.stacks | flamegraph.pl --hash > out.svg

# syscall tracing, more efficient than strace.
perf trace -p PID
# explain syscall times, ^C to stop collecting. "-s" for summary per thread
perf trace -s -p PID

# e.g., trace sendto
perf trace -e sendto -p PID
# you can see the time spent and the size of the data (len), but no argument details
# for tracing with argument details see next
```

## `strace` syscalls tracing

```sh
# Trace all syscalls
# -tt to display it with time, -T for <duration> in microseconds (us) at the end
strace -tt -T -p PID

# Trace and show only summary
strace -c -p PID
```

Other good options:

* `-P path`: Trace only system calls accessing path.  Multiple -P options can be used to specify several paths.
* `-Z`: Print only failed ones
* `-f`: follow children as well
* `--trace=syscall_set`:
   * `[syscall]`: Trace specific syscall, specified by its name (see syscalls(2) for a reference, but also see NOTES).
   * `/[regex]`: Trace only those system calls that match the regex.
   * `file`, `process`, `network`, `signal`, `ipc`, `desc` (descriptors), `memory` (use `-c` to first see useful syscalls to trace)


## `flamescope`

Good to pick specific subsection ranges of perf stacks

```sh
sudo perf record -F 99 -a -g -- sleep 10 # for example
sudo perf script --header > profiles/out.stacks
sudo chown [user]:[user] profiles/out.stacks
podman run --privileged --rm -it -v $(pwd)/profiles:/profiles:ro -p 5000:5000 prakashramesh/flamescope
# visit http://localhost:5000 and select the out.stacks file
```

## `profile` CPU profiling using timed sampling

Timer-based from BCC. Uses BPF to reduce overhead.

```sh
# sample 
profile -af 10 > out.stacks

# sample across CPUs, at 49Hz for 10secs piped to flamegraph
profile -F 49 10 -f | flamegraph.pl > out.svg
```

## `offcputime` Off-CPU profiling

```sh
# trace for 5 sec
offcputime 5
```

```sh
# to flamegraphs
offcputime -f 5 | flamegraph.pl --bgcolors=blue > out.svg
```

## `execsnoop` New process tracing

Trace `execve`

```sh
execsnoop
```

## `syscount` Syscall counting

Count system-wide syscalls by syscall

```sh
syscount
```

Count by process

```sh
syscount -P
```

## DSO Profiling

```sh
env LD_DEBUG=statistics /bin/echo "+++some test+++"
```

```sh

# tracks calls to PLT in order to profile
export LD_PROFILE=libfoo.so.1.2.3
export LD_PROFILE_OUTPUT=$(pwd)/prof_data
mkdir -p $LD_PROFILE_OUTPUT

#then use the following command to get reports as profiling happens 
sprof [-p|-q|-c] libfoo.so.1.2.3 $LD_PROFILE_OUTPUT/libfoo.so.1.2.3.profile
```

## `bpftrace` Signal tracing, i/o profiling, syscalls, lock analysis

### Signal tracing

```sh
bpftrace -e 't:syscalls:sys_enter_kill { time("%H:%M:%S "); printf("%s (PID %d) send a SIG %d to PID %d\n", comm, pid, args->sig, args->pid); }'
```

Same doable with `killsnoop`

### I/O socket profiling

```sh
# io buffer sizes
bpftrace -e 't:syscalls:sys_enter_recvfrom { @bytes = hist(args->size); }'
```

```sh
# actually received
bpftrace -e 't:syscalls:sys_exit_recvfrom { @bytes = hist(args->ret); }'
```

```sh
# latency in ms
bpftrace -e 't:syscalls:sys_enter_recvfrom { @ts[tid] = nsecs; }
   t:syscalls:sys_exit_recvfrom /@ts[tid]/ { @usecs = hist((nsecs - @ts[tid]) / 1000); delete(@ts[tid]); }'

# latency in ms, filtered by processes name
bpftrace -e 't:syscalls:sys_enter_recvfrom /comm == "mysqld"/ { @ts[tid] = nsecs; }
   t:syscalls:sys_exit_recvfrom /@ts[tid]/ { @usecs = hist((nsecs - @ts[tid]) / 1000); delete(@ts[tid]); }'

```

* tracepoints available: `/usr/share/bcc/tools/tplist`
* tracepoints arguments: `/sys/kernel/debug/tracing/events/[event_group]/[event]/format`

Docker command to give priviledges to trace tracepoints:
```sh
# this also allow for commands like perf stat
sudo podman run -v /sys/kernel/debug:/sys/kernel/debug:ro --privileged --network host --pid host --ipc host -it --rm quay.io/centos/centos:stream8 /bin/bash
```


### syscalls analysis

Overall performance analysis:
```
tracepoint:raw_syscalls:sys_enter
/comm == "ls"/
{
  @ts[tid, args->id] = nsecs;
}

tracepoint:raw_syscalls:sys_exit
/@ts[tid, args->id]/
{
  $start = @ts[tid, args->id];
  $end = nsecs;
  @histo[args->id] = hist($end-$start);
  @sum[args->id] = sum($end-$start);
  delete(@ts[tid, args->id]);
}

END
{
  clear(@ts);
  printf("\nsyscall names: ausyscall --dump\n")
}
```

Drilldown for one syscall `do_mmap`

```
kprobe:do_mmap
/comm == "ls"/
{
  @[ustack(perf,6),kstack(perf,6)] = count()
}

```

### Mutex Lock analysis

Mutex Lock contention

```
#!/usr/bin/bpftrace

BEGIN
{
	printf("Tracing libpthread mutex lock latency, Ctrl-C to end.\n");
}

uprobe:libc:pthread_mutex_lock
/$1 == 0 || pid == $1/
{
	@lock_start[tid] = nsecs;
	@lock_addr[tid] = arg0;
}

uretprobe:libc:pthread_mutex_lock
/($1 == 0 || pid == $1) && @lock_start[tid]/
{
	@lock_latency_ns[usym(@lock_addr[tid]), ustack(perf,7), comm] =
	    hist(nsecs - @lock_start[tid]);
	delete(@lock_start[tid]);
	delete(@lock_addr[tid]);
}

END
{
	clear(@lock_start);
	clear(@lock_addr);
}
```

For example, for java
```sh
jcmd [pid] Compiler.perfmap
./pmlock.bt [pid] > results.txt
```

Then, look into why by looking into threads holding the lock for too long:

```
#!/usr/bin/bpftrace

BEGIN
{
	printf("Tracing libpthread mutex held times, Ctrl-C to end.\n");
}

uprobe:libc:pthread_mutex_lock,
uprobe:libc:pthread_mutex_trylock
/$1 == 0 || pid == $1/
{
	@lock_addr[tid] = arg0;
}

uretprobe:libc:pthread_mutex_lock
/($1 == 0 || pid == $1) && @lock_addr[tid]/
{
	@held_start[pid, @lock_addr[tid]] = nsecs;
	delete(@lock_addr[tid]);
}

uretprobe:libc:pthread_mutex_trylock
/retval == 0 && ($1 == 0 || pid == $1) && @lock_addr[tid]/
{
	@held_start[pid, @lock_addr[tid]] = nsecs;
	delete(@lock_addr[tid]);
}

uprobe:libc:pthread_mutex_unlock
/($1 == 0 || pid == $1) && @held_start[pid, arg0]/
{
	@held_time_ns[usym(arg0), ustack(perf,5), comm] =
	    hist(nsecs - @held_start[pid, arg0]);
	delete(@held_start[pid, arg0]);
}

END
{
	clear(@lock_addr);
	clear(@held_start);
}
```

For example, for java
```sh
jcmd [pid] Compiler.perfmap
./pmheld.bt [pid] > results.txt
```

Another tool but without the perf mode stacks: `bpf-klockstat`

## Missing symbols and stacks


C/C++ missing stacks: compile with `-fno-omit-frame-pointer`.
C/C++ missing symbols: not use strip to reduce file size.


Java missing stacks: run with `-XX:+PreserveFramePointer`.
Java missing symbols (since hotspot jdk 17).

```sh
perf record -F 49 -p [pid] -g
jcmd [pid] Compiler.perfmap
perf script --header > out.stacks
...
```

```sh
jcmd [pid] Compiler.perfmap
bpftrace -e 'kprobe:do_readv /pid==[pid]/ { @[ustack(perf,6),kstack(perf,6)] = count(); }'
```



