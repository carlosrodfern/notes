# Example of Deflation/Inflation

1. An application performs 1 byte write to an existing file
2. The fs identifies the location as part of a 128kb fs record, which is not cached
3. The fs requests that the record be loaded from disk
4. The disk device layer breaks the 128 kb read into smaller reads suitable for the device
5. The disks perform multiple smaller reads, totalling 128kb
6. The fs now replaces the 1 byte in the record with the new byte
7. Later, the fs or kernel requesets that the 128 kb dirty record be written back to disk
8. The disks write the 128kb record (broken up if needed)
9. The fs writes new metadata of the file
10. The disks perform more writes

# Methodology

## Latency Analysis

Where to trace it:

* *Application*: Pros: best, understand also the context (async or sync). Cons: different techniques depending on the app.
* *Syscall interface*: Pros: well documented, observable. Cons: includes all fs types, including sockets, stats, which can confuse unless filtered; there are multiple syscalls for the same function, e.g. `read(2)`, `preadv(2)`, `preadv2(2)`.
* *VFS*: Pros: standard interface for all fs, one call for write: `vfs_write()`. Cons: traces all fs types, including non-storage ones, which then requires filtering.

For a system with high cache hit rate, the average is dominated by cache hit latency. Better to have distributions to detect outliers.

## Workload Characterization

Useful for capacity planning, benchmarking, and simulation. It helps to identify unnecessary work that can be eliminated.

Basic attributes:

* Operation rate and operation types
* File i/o throughput
* File i/o size (avg read size, write size)
* Read/write ratio
* Synchronous write ratio
* Random vs sequential file offset access

Tools:
* `bpftrace -e 'kfunc:vmlinux:vfs_read { @ = hist(args->count) }'`
* `bpftrace -e 'kfunc:vmlinux:vfs_read { @ = avg(args->count) }'`
* `bpftrace -e 'kfunc:vmlinux:vfs_write { @ = avg(args->count) }'`
* `bpf-vfsstat`

Advance attributes:

* Cache hit ration? Miss rate?
* Cache capcity and current usage
* Other cache stats (dentry, inode, buffer)
* Changes to fs tuning in the past
* App using the fs
* Files and dir being used, and how
* Errors
* Why fs i/o issued (call path)
* Degree the app directly (sync) requests fs i/o
* Distribution of i/o arrival times
* Average fs operation latency
* High latency outliers
* Distribution of latency 
* Cgroups

Event tracing is the best tool to see these things, however, they produce overhead. Better to start with overall stats before drilling down with tracing.

## Performance Monitoring

Operation rate and latency.

Operation latency may be monitored per second average, and include other values like maximum and stdev. Ideally, being able to inspect full distribution of latency, using histograms or heat maps.

Both may be recorded for each operation type (read, write, stat, open, close, etc...).

## Static Performance Tunning

Check configurations of the fs: mounted, actively used, record size, access timestamp disabled, compression, encryption, caches (page, dentry, inode, buffer) config, number of storage devices and their config, fs type, patches, cgroups, workload separation

## Micro-benchmarking

Factors to be tested:

* Operation types: rate of reads/writes/others
* i/o size: size issued
* Offset pattern: sequential, random
* Random-access pattern: uniform, random, pareto distribution
* Write type: async or sync (`O_SYNC`)
* Working set size: how well it fits into the cache
* Concurrency: number of parallel i/o
* Memory mapping: File access via `mmap(2)` vs `read(2)/write(2)`
* Cache state: cold or warm
* File system tunables: compression, deduplication, etc...

# Observability Tools

## `mount`

list mounted fs and their option flags

## `free`

`free -mw` in megabytes and wide (separating buffer from page cache)

The `available` column shows the available memory applications can use without needing to swap, taking into account even memory that can't be reclamed immediately.

`top` and `vmstat` also shows buffer and page cache.

## `sar`

`sar -v` shows number of unused dentry count, number of file handles, inodes and pseudo-terminals in use.

`sar -r` prints buffer and cache as well.

## `slabtop`

kernel slab caches which  includes "buffer head" used by the buffer cache, dentry cache, inode cache, and inode cache.

## `opensnoop`

Traces open files. Useful to detect location of data, logs, and configuration files, or the errors looking for them.

## `filetop`

Most frequent read/written files. For workload characterization

## `cachestat`

Shows page hit/miss stats. Based on kprobes, so it is fragile.

## `ext4dist`/`ext4slower` (xfs, zfs, btrfs, nfs)

To use to together to troubleshoot latency issues.

1) Show latency distribution per operation: `read(2)`, `write(2)`, `open(2)` and `fsync(2)`.

2) Traces slower operations and prints them.

## `bpftrace`

```sh
# trace openat(2) with process name
bpftrace -e 't:syscalls:sys_enter_openat { printf("%s %s\n", comm, str(args->filename)); }'

# count read syscalls by type
bpftrace -e 't:syscalls:sys_enter_*read* { @[probe] = count(); }'

# count write syscalls by type
bpftrace -e 't:syscalls:sys_enter_*write* { @[probe] = count(); }'

# show the distrib of read(2) request sizes (bytes)
bpftrace -e 't:syscalls:sys_enter_read { @ = hist(args->count); }'

# show the distrib of read(2) read sizes (bytes) and errors (-1)
bpftrace -e 't:syscalls:sys_exit_read { @ = hist(args->ret); }'

# show count read(2) errors by code
bpftrace -e 't:syscalls:sys_exit_read /args->ret < 0/ { @[- args->ret] = count(); }'

# count vfs calls
bpftrace -e 'kfunc:vmlinux:vfs_* { @[probe] = count(); }'

# count vfs calls by pid
bpftrace -e 'kfunc:vmlinux:vfs_* /pid == 1234/ { @[probe] = count(); }'

# count btrfs tracepoints (can be done for ext4 and xfs)
bpftrace -e 't:btrfs:* { @[probe] = count(); }'
```

vfs read latency distribution by type of file system (including sockfs, ext4, etc..). It can be done also for other operations like `vfs_readv()`, `vfs_write()`, and `vfs_writev()`.
```
#!/usr/bin/bpftrace

BEGIN
{
    printf("Tracing vfs_read() by type... Hit Ctrl-C to end.\n");
}

kfunc:vmlinux:vfs_read
{
    @file[tid] = str(args->file->f_inode->i_sb->s_type->name);
    @ts[tid] = nsecs;
}

kretfunc:vmlinux:vfs_read
/@ts[tid]/
{
    @us[@file[tid]] = hist((nsecs - @ts[tid]) / 1000);
    delete(@file[tid]); delete(@ts[tid])
}

END
{
    clear(@file); clear(@ts);
}
```

## Other tools

See bcc and bpf tools in linux install.

Some:

* `statsnoop` `syncsnoop`,
* `vfscount`, `vfsstat`, `vfssize`
* `fileslower` (shows slow file read/writes)
* `filetype` (shows vfs reads/writes by file system type)
* `ioprofile` (counts stacks on i/o to show code path)
* `dcstat`, `dcsnoop` (shows directory cache stats and lookups)
* `icstat` (shows inode cache stats)
* `readahead` (shows read ahead hits and efficiency) 

# Experimentation

Tools: `sysbench`, `fio`.

Flushing caches for benchmarking:

```sh
# free page cache
echo 1 > /proc/sys/vm/drop_caches

# free reclaimable slab objects
echo 2 > /proc/sys/vm/drop_caches

# free slab objects and page cache
echo 3 > /proc/sys/vm/drop_caches
```

# Tuning

* `posix_fadvise()`: Predeclare an access pattern for file data. The kernel can use it to improve performance.
* `madvise()`: Predeclare an access pattern for memory mapping. The kernel can use it to improve performance.
* `notime` mount option for ext4
* Other tunables for the file system: `/sys/fs/<type>/<device>/`
