# Methodology

## USE Method

* **Utilization**: how much physical and virtual memory is in used
* **Saturation**: degree of page scanning, paging, swapping, and OOM Killer
* **Errors**: software or hardware errors

## Characterizing Usage

* System-wide physical and virtual memory utilization
* Degree of saturation
* Kernel and fs cache mem usage
* per-process physical and virtual mem usage
* Usage of memory resource controls (cgroups)

Tools:
* `sar -r ALL`
* `sar -q MEM`
* `sar -B`
* `pidstat -r`


## Performance Monitoring

* **Utilization**: Percent used
* **Saturation**: Swapping, OOM killing

## Leak detection

* `memleak` for growth vs leak analysis.

## Static Performance Tuning

* Memory total
* App memory configuration (max heap, etc...)
* Which memory alloc the app uses
* Memory speed, DDR5?
* memtester ever run?
* NUMA vs UMA, is OS NUMA-aware, NUMA tunables?
* Memory attached to same socket or split across?
* Number of memory busses?
* CPU cache sizes? TLB?
* BIOS settings?
* Are large pages configured and used?
* Overcommit available and configured?
* Other tunables used?
* cgroups?

## Memory shrinking

Method to calculate WSS. Available main memory for a process is progressively reduced while measuring performance and swapping: the point where performance sharply degrades, and swapping greatly increases shows when the WSS no longer fit into main memory.

`wss` BCC tool can also be used to measure WSS, but it is out of date.

# Observability Tools

## `vmstat`

`vmstat -SM 1`, `-S` for unit (M for 1024 based)

`free` column is idle memory, which is less than the available memory to processes.

## PSI

`cat /proc/pressure/memory`

## `swapon`

That.

## `sar`

* `-B` paging statis
* `-H` huge pages stats
* `-r` memory utilization (`-r ALL` for more information including kernel usage)
* `-S` swap space stats
* `-W` swappign stats

## `slabtop`

`slabtop -sc` sorting by cache size

It is also available with `vmstat -m`

## `numastat`

stats for NUMA.

## `ps`

`ps aux`.

* `%MEM` column is RSS as % of total physical.
* `RSS`
* `VSZ` virtual mem size

## `top`

`top -o %MEM` to sort by MEM utilization.

## `pmap`

Memory mappings of a process in details, including virtual mem (Kbytes column), RSS, mode, etc...


`pmap -x [pid]`. First wave of `[anon] is the heap.


`pmap -X [pid]` includes kernel stuff.

## `perf`

```sh
# sample page faults (RSS growth) with strack traces system wide
perf record -e page-faults -a -g

# sample page faults with stack traces for PID for 10 secs, count one every one event
perf record -e page-faults -c 1 -p [pid] -g -- sleep 10

# record heap growth via brk(2)
perf record -e syscalls:sys_enter_brk -a -g

# record page migrations on NUMA:
perf record -e migrate:mm_migrate_pages -a

# count "kmem", "vmscan" or "compaction" events, printing a report every second
perf stat -e '<event name>:*' -a -I 1000

# trace kswapd wakeup events with stack traces
perf record -e vmscan:mm_vmscan_wakeup_kswapd -ag

# profile memory access for a given command, only on Intel CPUs
perf mem record [command]
perf mem report
```

Page faults (minor faults) indicate RSS increasing.

Flame Graphs to identify code paths causing the increase

```sh
perf record -e page-faults -a -g -- sleep 60
perf script --header > out.stacks
stackcollapse-perf.pl < out.stacks | flamegraph.pl --hash --color=green --count=pages > out.svg
```

```sh
bpftrace -e 't:exceptions:page_fault_user { @[ustack, comm] = count(); }' > out.stacks
stackcollapse-bpftrace.pl < out.stacks | flamegraph.pl --hash --color=green --count=pages > out.svg
```

## `drsnoop`

BCC tool, to measure latency of direct reclaims: `drsnoop -T`


## `bpftrace`

```sh
# sum malloc request bytes by user stack and process (high overhead)
bpftrace -e 'uprobe:/usr/lib64/libc.so.6:malloc { @[ustack, comm] = sum(arg0); }'

# sum malloc request bytes by user stack and pid (high overhead)
bpftrace -e 'uprobe:/usr/lib64/libc.so.6:malloc /pid == [pid]/ { @[ustack] = sum(arg0); }'

# histogram
bpftrace -e 'uprobe:/usr/lib64/libc.so.6:malloc /pid == [pid]/ { @[ustack] = hist(arg0); }'

# kernel kmem cache alloc. kmem_cache_alloc operates on a specific slab cache, kmem_alloc on a slab cache
bpftrace -e 't:kmem:kmem_cache_alloc { @bytes[kstack] = sum(args->bytes_alloc); }'

# count page faults by process:
bpftrace -e 'software:page-faults { @[comm, pid] = count(); }'

# by user level stack trace:
bpftrace -e 't:exceptions:page_fault_user { @[ustack, comm] = count(); }'

# count vmscan
bpftrace -e 't:vmscan:* { @[probe] = count(); }'

# count swapins by process 
bpftrace -e 'kprobe:swap_readpage { @[comm, pid] = count(); }'

# count page migrations
bpftrace -e 't:migrate:mm_migrate_pages { @ = count(); }'

# trace compation events
bpftrace -e 't:compaction:mm_compaction_begin { time(); }'

# list all memory subsystem tracepoints, same for t:kmem:* and uprobe:/usr/lib64/libc.so.6:*
bpftrace -l 't:*:mm_*'

```

## Flame Graphs

```sh
# bytes per stack
bpftrace -e 'uprobe:/usr/lib64/libc.so.6:malloc /pid == [pid]/ { @[ustack] = sum(arg0); }' > out.stacks
./stackcollapse-bpftrace.pl < out.stacks | ./flamegraph.pl --hash --color=green --count=bytes > out.svg

# page fault per stack
bpftrace -e 't:exceptions:page_fault_user /pid == [pid]/ { @[ustack] = count(); }' > out.stacks
./stackcollapse-bpftrace.pl < out.stacks | ./flamegraph.pl --hash --color=green --count=pagefaults > out.svg
```

List arguments of tracepoints:

```sh
# example, of kmem tracepoints
bpftrace -lv 't:kmem:*'
```

IMPORTANT: Memory tracing is usually heavy on the system because the number of events/sec in many cases is pretty high.


## `dmidecode`

Information about memory bank.

# Tuning

## Tunable

https://www.kernel.org/doc/html/latest/admin-guide/sysctl/vm.html

`vm.min_free_kbytes` is set dynamically as a fraction of the main memory. When the free list is bellow this number kswapd comes foreground scanning, synchronously reclaiming free pages, causing latency issues. When it is greater than that number kswapd remains in the background, but its aggressiveness depends on whether it is above the high pages region (min_free_kbytesx2 - min_free_kbytesx3), in the high pages region, or in the low pages region (min_free_kbytes - min_free_kbytesx2). Increasing it can help to avoid OOMKill but increases kernel work during memory pressure, but reducing it frees memory for apps.

`vm.overcommit_memory` can be set to 2 to disable it and avoid cases of OOM.

`vm.swappiness` may be desirable to set to 0 to keep app memory at the expense of page caches, depending on the type of load.

`kernel.numa_balancing` can lead to overly addressive NUMA scanning using too much CPU (kernel 3+), but newer kernels have improved it, and provide a tunable called `kernel.numa_balancing_scan_size_mb` for adjusting aggressiveness.

## Multiple Page Sizes

Large pages can improve performance by improving the hit ration of the TLB. Page sizes go from 4KB default to 2MB.

```sh
# create 50 huge pages
echo 50 > /proc/sys/vm/nr_hugepages
grep Huge /proc/meminfo
```

How to consume Huge pages:

1) Shared memory segments, `SHM_HUGETLBS` flag in `shmget(2)`.
2) Map to fs: `mkdir /mnt/hugetlbsfs`, `mount -t hugetlbfs none /mnt/hugetlbfs -o pagesize=2048K`
3) `MAP_ANONYMOUS|MAP_HUGETLB` flags to `mmap(2)`, and use `libhugetlbfs`.
4) _transparent huge pages_ (THP), which promotes and demotes normal pages to huge.

## Allocators

Using different allocators specified at compilation time or at runtime with `LD_PRELOAD`.

## NUMA Binding

Bind process memory alloc to node 0: `numactl --membind=0 [pid]`. Use in combination with `--physcpubind` option. Better to use both, the NUMA binding and the CPU binding.

## Resource Controls

With `cgroups`.

https://www.redhat.com/sysadmin/cgroups-part-one
https://www.redhat.com/sysadmin/cgroups-part-two
https://www.redhat.com/sysadmin/cgroups-part-three
https://www.redhat.com/sysadmin/cgroups-part-four

