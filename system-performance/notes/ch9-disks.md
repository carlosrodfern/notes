# Methodology

## USE Method

Disk Devices:

* **Utilization**: the time the device was busy
* **Saturation**: The degree to which io is waiting in a queue
* **Errors**: Device errors

Disk Controllers:

* **Utilization**: current vs maximum throughput, and the same for operation rate
* **Saturation**: the degree to which io is wating due to controller saturation
* **Errors**: controller errors

`iostat` does not provider per-controller metrics but knowing the architecture of the hardware and having the metrics per device can help to figure it out by summing, and comparing with the hardware limits

## Performance Monitoring

* Disk Utilization
* Response time

## Workload Characterization

Averages and maximums of:

* io rate
* io throughput
* io size
* read/write ratio
* random vs sequential

Tools:
* `sar -d`


## Latency Analysis

* per-interval io avg: typically reported by sysstats
* full io distributions: histograms or heat maps
* per-io latency values

Stack latency analysis

1. Application
2. System Calls
3. VFS
4. File System
5. Block Device Interface
6. Drivers
7. Disk

## Micro-Benchmarking

Factors to include:

* **Direction**: read or writes
* **Disk offset pattern**: random vs sequential
* **Range of offsets**: full disk or tight ranges
* **io size**: 512B up to 1MB
* **Concurrency**: number of io in flight, or threads performing io
* **Number of devices**: single disk tests, or multiple disks (to explore controller and bus limmits)

Goal:

* **Maxium disk throughput**: 128KB or 1MB reads, sequential
* **Maximum disk operation rate (iops)**: 512B reads, offset 0 only (intended to cache the data on the on-disk cache)
* **Maximum disk random reads (iops)**: 512B reads, random offsets
* **Read latency profile** (avg usecs): Sequential reads, repeat for 512B, 1KB, 2KB, 4KB, and so on
* **Random io latency profile** (avg usecs): 512B reads, repeat for full offset span, beginning offsets only, end offsets only.

* **Maxium disk throughput**: 128KB or 1MB writes, sequential
* **Maximum disk operation rate (iops)**: 512B writes, offset 0 only
* **Maximum disk random writes (iops)**: 512B writes, random offsets
* **Write latency profile** (avg usecs): Sequential writes, repeat for 512B, 1KB, 2KB, 4KB, and so on
* **Random io latency profile** (avg usecs): 512B writes, repeat for full offset span, beginning offsets only, end offsets only.

Disk controllers:

* **Maximum controller throughput**: 128KB, offset 0 only
* **Maximum controller operation rate(iops)**: 512B reads, offset 0 only

It might take a dozen disks to find the controllers limits.

# Observability Tools

RWBS flag description

* R: read
* W: write
* M: metadata
* S: synchronous
* A: read ahead
* F: flush
* D: discard
* E: erase
* N: none

They can be combined, e.g., `WS` is synchronous writes.

## `iostat`

Stats at the disk level (no fs level).

Useful options:

* `-z`: skip zero activity
* `-x`: extended
* `-s`: short output (not divided by reads/writes)
* `-t`: include timestamp (for copy pasting results with time on it)

See `man` for columns. Key columns:

* `tps`: iops
* `kB/s`: throughput
* `rqm/s`: requests queued and merged per second
* `await`: avg io response time including the time on the kernel and on the device
* `aqu-sz`: avg number of requests both waiting on the driver queue and active on device
* `areq-size`: avg reuqest size in KB.
* `%util`: Utilization (valid for serial devices, but for  devices  serving  requests in parallel, such as RAID arrays and modern SSDs, this number does not reflect their performance limits., see `man`)

Splitting by read/write/discard/flush requests is better (basically remove the `-s` option).


## `sar`

`sar -d 1` for the disk reprot.

## `PSI`

`cat /proc/pressure/io`

Once you find out there is a io issue, you can find the root cause with `pidstat`.

## `pidstat`

`pidstat -d 1`


## `perf`

`perf list 'block:*'`

e.g.

```sh
# issues
perf record -e block:block_rq_issue -a -g sleep 10
perf script --header
# top line per process gives you in that order:
# process name, thread id, cpu id, timestamp, event name;
# then specific to that syscall: disk major and minor number, io type,
# io size, io command string, sector addr, number of sectors and process name.
```

```sh
# You can get the output into a file to process it later (e.g, correlating by process thread id and then using the
# timestamps to calculate latency)
perf record -e block:block_rq_issue,block:block_rq_complete -a sleep 60
perf script --header > output.txt
```

Oneliners

```sh
# trace all block completions of size greater than 100KB
perf record -e block:block_rq_complete --filter 'nr_sector > 200'

# trace all block completions, synchronous writes only
perf record -e block:block_rq_complete --filter 'rwbs == "WS"'

# trace all block completions, all writes
perf record -e block:block_rq_complete --filter 'rwbs == "*W*"'
```

## `biolatency`

`biolatency -m -F 10 1`

Options

* `-m`: output in msecs
* `-Q`: include OS queued io time
* `-F`: histogram per io flag set
* `-D`: histogram per disk device

## `biosnoop`

io completions, per device and process, including latency (which includes queue time). `-Q` flag separates block queue time from the block service time (which includes disk queue and service time).

## `biotop`

`top` like command for disks.

## `biostacks`

`biostacks.bt`
Traces the block io request time (from os enqueue to device completion) with the io initialization stack trace.


## `bpftrace`

```sh
# count block io events
bpftrace -e 't:block:* { @[probe] = count(); }'

# summarize block io issue size as a histogram:
bpftrace -e 't:block:block_rq_issue { @bytes = hist(args->bytes); }'

# summarize block io insert into the queue size as a histogram:
bpftrace -e 't:block:block_rq_insert { @bytes = hist(args->bytes); }'

# same as above but by process and flag
bpftrace -e 't:block:block_rq_insert { @[comm, args->rwbs] = hist(args->bytes); }'

# count block io type flags
bpftrace -e 't:block:block_rq_issue { @[args->rwbs] = count(); }'

# trace block io errors with device and io type
bpftrace -e 't:block:block_rq_complete /args->error/ {
    printf("dev %d type %s error %d\n", args->dev, args->rwbs, args->error);
}'
```

biolatency on tracepoints:

```
#!/bin/bpftrace

BEGIN
{
    printf("Tracing block device I/O... Hit Ctrl-C to end.\n");
}

tracepoint:block:block_rq_issue
{
    @start[args->dev, args->sector] = nsecs;
}

tracepoint:block:block_rq_complete
/@start[args->dev,args->sector]/
{
    @usecs = hist((nsecs - @start[args->dev, args->sector]) / 1000);
    delete(@start[args->dev, args->sector]);
}

END
{
    clear(@start);
}
```

# Experimentation

```sh
# reads, while having iostat running on another window
dd if=/dev/sda1 of=/dev/null bs=1024k count=1k

# writes, while having iostat running on another window, skipping buffer
dd if=/dev/zero of=out1 bs=1024k count=1000 oflag=direct
```

```sh
# Example of sequential writes, with 4k size blocks and total file size of 30G, and 4 threads doing the io
# Generate files for the testing
sysbench --threads=4 fileio --file-block-size=4096 --file-total-size=30G --file-test-mode=seqrw prepare
# Run the test and output stats
sysbench --threads=4 fileio --file-block-size=4096 --file-total-size=30G --file-test-mode=seqrw run
# Cleanup files
sysbench --threads=4 fileio --file-block-size=4096 --file-total-size=30G --file-test-mode=seqrw cleanup
```

```sh
# Example using the direct flag with sequential read test (ommiting the prepare and the cleanup for brevity)
sysbench --threads=4 fileio --file-block-size=4096 --file-total-size=30G --file-test-mode=seqrd --file-extra-flags=direct run
```

```sh
# test cache reads, and buffered disk reads.
hdparm -Tt /dev/sdb
```

`fio` with the `--direct=true` flag.

```
fio some-fio-spec-file.fio
```

# Tuning

`man ionice`, e.g. `ionice -c 3 -p 1234`.

```
-c, --class class
           Specify the name or number of the scheduling class to use; 0 for none, 1 for realtime, 2 for best-effort, 3 for
           idle (only when disk has been idle)
```

Queue sysfs:

* `/sys/block/*/queue/scheduler`: io scheduler. Better change permanently with `tuned`.
* `/sys/block/*/queue/nr_requests`: number of read/write requests that can be allocated to the block layer.
* `/sys/block/*/queue/read_ahead_kb`: maximum read ahead KB for fs to request.
* `/sys/block/*/queue/minimum_io_size` (RO): minimum preferred io size.
* `/sys/block/*/queue/max_hw_sectors_kb` (RO): maximum number of kilobytes supported in a single data transfer.
* `/sys/block/*/queue/max_sectors_kb`: maximum number of kilobytes that the block layer will allow for a filesystem request. Must be smaller than or equal to the maximum size allowed by the hardware.

See https://www.kernel.org/doc/html/v5.8/block/queue-sysfs.html
