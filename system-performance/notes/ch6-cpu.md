# Methodologies

## USE Method

* **Utilization**: Time the CPU was busy
* **Saturation**: Degree to which runnable threads are queued waiting their turn on-CPU.
* **Errors**: CPU errors, including correctable errors.

## Workload Characterization

* CPU load averages (utilization + saturation)
* User vs Sys time ratio
* Syscall rate
* Voluntary Context Switch rate
* Interrupt rate

CPU bound uses a lot of user time.

I/O bound uses a lot of sys time. Syscall and interrupt rate can explain the sys time. Higher Voluntary Context Switch are characteristic of i/o-bound loads.

Tools:

* `sar -u` system-wide CPU utilization, and user/sys ratio
* `sar -q LOAD` queue length
* `sar -w` context switches per second
* `sar -I SUM` interrupts rate
* `perf stat -e raw_syscalls:sys_enter -a -I 1000 sleep 5` syscall rate per sec

## Performance Monitoring

* **Utilization**: Percent busy
* **Saturation**: Either run-queue length or scheduler latency

Utilization should be monitored per CPU basis to identify thread scalability issues.

## Profiling

Time-based (lower overhead) or function tracing (higher overhead).

Samplying can generate lots of data to store on disk in production. BPF based one summarizes in memory.
`perf` one writes to disk and can create i/o perturbances, but however, it can be used for several types of analysis (flamegraph, flamescope, etc...).

Interpretation of flamegraphs:

1. Look top-down for large plateaus. Looking for those functions that may be optimized or skipped.
2. Look bottom-up to understand code hierarchy. Looking for parent functions that can be skipped, or optimized.
3. Look more carefully top-down for scatted but common CPU usage. Small frames repeated many times and put together represent a lof of CPU usage, e.g. lock contentions.

## Priority Tuning

* Using `nice`: looking for lower priority processes and start them with lower priority.
* Scheduler classes: just be careful with RT, and locking yourself out of the computer in case the RT process has a bug and gets into an infinite loop.


## Resource Controls

Fix limits

## CPU Binding

Using cpusets to ensure caches stay warm for processes that share memory.

# Observability Tools

## systats

`vmstat`, `mpstat`, `sar`, `ps`, `top` (it hads like 1ms overhead, `atop` better to detect short-living process),
`pidstat`, `time`.

## `perf`

`perf record -F 99 [command]`

`perf record -F 99 -a -g -- sleep 10`

`perf list` list events that can be monitored.

`perf record -e [event] -a` record event

`perf record -e [event] -a -g -- sleep 10` record event with stack traces

`perf record -e migrations -a -- sleep 10` record cpu migrations for 10 secs

`perf report -n --stdio` show report with count and percentages

`perf stat -a -- sleep 5` show PMC statistics system-wide for 5 secs

`perf stat -e [events,]` show system-wide events stats

`perf stat -e [events,] [command]` show stats for a command. E.g. `LLC-loads,LLC-load-misses,LLC-stores,LLC-prefetches`

`perf stat -e sched:sched_switch -a -I 1000` show rate of context switches per-second (voluntary and involuntary).

`perf stat -e sched:sched_switch --filter 'prev_state != 0' -a -I 1000` show voluntary context switching (`prev_state` different than `TASK_RUNNING`)

`perf sched record -- sleep 10` & `perf sched latency` show scheduler latency for 10 seconds.

`perf record -F 99 -a -g -- sleep 10` & `perf script report flamegraph` to generate flamegraphs

## `cpudist`

Distribution of on-CPU time per task.

`cpudist 10 1`

## `runqlat`

run queue latency distribution

Kind of heavy, use `runqlen` instead as much as possible

`runqlat 10 1`

## `runqlen`

Queue length sample counts distribution

`runqlen 10 1`

## `softirqs`, `hardirqs`

Distribution of CPU time spent on interruptions.

## `bpftrace`

To use with eBPF technologies

`bpftrace -l 'tracepoint:sched:*'` list available tracepoints

`bpftrace -e '[expression]'` evaluate expression

```sh
# trace new processes with arguments
bpftrace -e 't:syscalls:sys_enter_execve { join(args->argv); }'

# count syscalls by process:
bpftrace -e 't:raw_syscalls:sys_enter { @[pid, comm] = count(); }'

# count syscalls by syscall probe name
bpftrace -e 't:syscalls:sys_enter_* { @[probe] = count(); }'

# sample running process names at 99hz
bpftrace -e 'profile:hz:99 { @[comm] = count(); }'

```

## `cpupower`

```sh
# stats about power states and usage.
cpupower idle-info
```

# Experimentation

## `sysbench`

```sh
# example
sysbench --num-threads=8 --test=cpu --cpu-max-prime=100000 run
```

See `sysbench --help` or `sysbench cpu help` for more details.

# Tuning

## Compiler Options

That

## Scheduling Priority and class

```sh
# run command with lower priority (-20..+19)
nice -n 19 [command]
```


```sh
# to run a command on a specific scheduling class
chrt -b [command]
```

## Scheduler Options

* `CONFIG_*` options
* `sysctl` options for `kernel.sched_*`

See also the tuned project for profiles

## CPU Binding

```sh
# set cpu affinity for pid for cpu 7 to 10.
taskset -pc 7-10 [pid]
```

See also `numactl(8)` for CPU binding and memory node binding

## Exclusive CPU sets

Assign exclusively CPUs to a process group.

```sh
mount -t cgroup -ocpuset cpuset /sys/fs/cgroup/cpuset # may not be necessary
mkdir /sys/fs/cgroup/cpuset/prodset # new cpu set called prodset
echo 7-10 > /sys/fs/cgroup/cpuset/prodset/cpuset.cpus # assign cpu 7-10
echo 1 > /sys/fs/cgroup/cpuset/prodset/cpuset.cpu_exclusive # make it exclusive
echo [pid] > /sys/fs/cgroup/cpuset/prodset/tasks # add pid to prodset
```

## Resource control

via cgroups

