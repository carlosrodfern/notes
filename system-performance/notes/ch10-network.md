# Methodology

## USE Method

* **Utilization**: The time the interface was busy sending or receiving frames
* **Saturation**: The degree of extra queueing, buffering, or blocking due to fully utilized interface
* **Errors**: For receive: bad checksum, frame too short (less than the data link header) or too long, collisions (unlikely with switched net), for transmit: late collisions (bad wiring)


Saturation of the network interface is difficult to measure. Some buffering is normal, as app can send data much more quickly than an interface can transmit. It may be possible to measure as the time app threads spend blocked on network sends, which will increase as saturation increases.

Retrans at the TCP are usually available as stats. It can indicate net saturation but could also be at any hop. SYN Retrans can be the server saturated.

## Workload Characterization

* Network interface throughput: rx/tx, bytes per second
* Network interface iops: rx/tx, frames per second
* TCP connection rate: active/passive, conn per seconds

Tools:
* `sar -n DEV`
* `sar -n TCP`

## Latency Analysis

See previous major section for different types of latencies. 

A typical source of issues is the latency outliers caused by TCP retrans.

On Linux, latency can be measured using socket options like `SO_TIMESTAMP` for incoming packet time, and `SO_TIMESTAMPING` for per-event timestamps. The later can identify transmission delays, network RTT, and inter-stack latencies.

## Performance Monitoring

Key metrics:

* **Throughput**: rx/tx per sec per interface
* **Connections**: TCP connections per sec, (network load)
* **Errors**: Including dropped packet counters
* **TCP retrans**: To record for correlation with network issues
* **TCP out-of-order packets**: Can cause performance problems.

## Packet sniffing

see `tcpdump`. High overhead to run

## TCP Analysis

* Usage of send/receive buffers
* Usage of TCP backlog queues
* Kernel drops due to backlog queue being full
* Congestion window size, including zero-size advertisements
* SYN received during a TCP `TIME_WAIT` interval

## Micro-Benchmarking

`iperf`

Typical factors to be tested:

* **Direction**: send/receive
* **Protocol**: TCP/UDP, and port
* **Number of threads**
* **Buffer size**
* **Interface MTU size**

# Observability Tools

## `ss`

Socket statistics tool

```sh
# -t: tcp
# -i: tcp internal info
# -e: extended socket info
# -p: process info
# -m: memory usage
ss -tiepm
```

Output:

* `"<processname>",pid=<pid>`
* `fd=<file descriptor>`
* `rto:<number>` TCP retrans timeout in ms
* `rtt:<avg>/<std>` average RTT, with mean dev
* `mss:<number>` maximum segment size in bytes
* `cwnd:<number>` congestion window size: <number> x MSS
* `bytes_acked:<number>` bytes successfully trans
* `bytes_received:<number>` bytes received
* `bbr:...` BBR congestion control stats
* `pacing_rate <number>Mbps` pacing rate
* `app_limited` shows that the congestion window is not fully utilized, suggesting that the connection is application-bound. Options are `app_limited`, `rwnd_limited:<number>ms` (limited by receive window, including time in ms), `sndbuf_limited:<number>ms` (limited by the send buffer, including the time in ms)
* `minrtt:<number>` minimum RTT in ms. Compare with avg and std to have an idea of network congestion.

## `ip`

tool for managing routing, net dev, interfaces, and tunnels.

```sh
# print stats on interfaces (link)
ip -s -s link

# print route tables
ip route

# watch for net link messages
ip monitor
```

## `ifconfig`

Shows some error stats per link

## `nstat`

```sh
# display metrics maintained by the kernel
nstat -s
```

## `netstat`

```sh
# stack stats
netstat -s

# interface stats
netstat -i
```

Some example of what to look for with `netstat -i`

* High rate of forwarded vs total packets. Is server supposed to be forwarding packets?
* Passive connection openings: can show load in terms of client connections
* A high rate of seg retrans vs seg sent out. Can show unreliable net, which could be expected with clients.
* TCP Syn Retrans: which can be caused by the remote endpoint dropping SYNs from the listen backlog due to load
* Packets pruned from the receive queue because of socket buffer overrun. This is a sign of net saturation, and may be fixable by increasing socket buffer, assuming there are enough resources for the app.

## `sar`

```sh
# options:
# DEV link stats
# EDEV link errors
# IP ip datagram stats
# EIP ip errors 
# TCP tcp stats
# ETCP tcp errors
# SOCK socket usage

sar -n <option,>

```

## `nicstat`

prints NIC throughput and utilization

```sh
# first output is a summary since boot, then the interval summary follows
nicstat -z 1
# -z to skip zeros lines
# -t to print TCP stats
```

## `ethtool`

Displays static configuration with `-i` and `-k` and also prints driver stats with `-S`

```sh
# driver stats
ethtool -S eth0

# -i driver details
# -k interface tunables
ethtool -i eth0
ethtool -k eth0
```

## `tcplife`

Traces the lifespan of TCP sessions

## `tcptop`

Shows top processes using TCP.

## `tcpretrans`

Traces TCP retrans, showing IP/port, and TCP state.

A few per second is low rate.

A high rate in `ESTABLISHED` state can point to an external network problem. A high rate in `SYN_SENT` state can point to an overloaded server application not consuming its `SYN` backlog fast enough.

## `bpftrace`

```sh
# count socket accept by pid/comm
bpftrace -e 't:syscalls:sys_enter_accept* { @[pid, comm] = count(); }'

# count socket connect by pid/comm
bpftrace -e 't:syscalls:sys_enter_connect { @[pid, comm] = count(); }'

# count socket by user stack trace
bpftrace -e 't:syscalls:sys_enter_connect { @[ustack, comm] = count(); }'

# count socket rx/tx by on-cpu pid/comm
bpftrace -e 'k:sock_sendmsg,k:sock_recvmsg { @[func, pid, comm] = count(); }'

# count tcp connects by on-cpu pid/comm
bpftrace -e 'k:tcp_v*_connect { @[pid, comm] = count(); }'

# count TCP accepts by on-cpu pid/comm
bpftrace -e 'k:inet_csk_accept { @[pid, comm] = count(); }'

# count tcp rx/tx by on-cpu pid/comm
bpftrace -e 'k:tcp_sendmsg,k:tcp_recvmsg { @[func, pid, comm] = count(); }'

# tcp send bytes as a histogram
bpftrace -e 'k:tcp_sendmsg { @send_bytes = hist(arg2); }'

# tcp receive bytes as a histogram
bpftrace -e 'k:tcp_recvmsg /retval >= 0/ { @recv_bytes = hist(retval); }'

# count tcp retrans by type, remote host, and port (ip4)
bpftrace -e 't:tcp:tcp_retransmit_* { @[probe, ntop(2, args->daddr), args->dport] = count(); }'

# count udp rx/tx by on-cpu pid/comm
bpftrace -e 'k:udp*_sendmsg,k:udp*_recvmsg { @[func, pid, comm] = count(); }'

# udp send bytes as hist
bpftrace -e 'k:upd_sendmsg { @send_bytes = hist(arg2); }'

# udp receive bytes as hist
bpftrace -e 'k:upd_recvmsg /retval >= 0/ { @recv_bytes = hist(retval); }'

# show receive cpu hist for each device
bpftrace -e 't:net:netif_receive_skb { @[str(args->name)] = lhist(cpu, 0, 128, 1); }'
```

Network events tracing at the socket layer has the advantage that the responsible process is still on cpu, making it easier to identify.


Tracepoints available from kernel 5.3:

```sh
# socket tracepoints
bpftrace -l 't:sock:*'

# tcp tracepoints
bpftrace -l 't:tcp:*'
```

**tcpsynbl.bt**: instrumenting TCP example using kprobes. It shows the length of the `listen(2)` backlog broken down by queue length when a `SYN` arrives, counting `SYN`s that found such length. It also show the limit of the queue on the histogram key (i.e. `@backlog[<limit>]`.


## `tcpdump`

To capture and inspect network packets.

```sh
# dump packets from the eth4 interface to a file for later inspection
tcpdump -i eth4 -w /tmp/out.tcpdump

# inspect packets from a dump file
# -n: do not resolve ip addresses as hostnames
tcpdump -nr /tmp/out.tcpdump
# other options
# -e: link-layer headers
# -vvv: verbose details
# -ttt: times between packets
# -X: content
```


# Experimentation

## `ping`

RTT, using ICMP echo.

## `traceroute`

Sends a series of test packets to experimentally determine the current route to a host.

Defaults to ICMP, but can use TCP instead with the `-T` option.

Path can change even during the test.

## `iperf`

To micro-benchmark throughput

```sh
# server side, with 128KB of socket buffer, from the default of 8KB
iperf -s -l 128k

# client side
# -c <host>: host
# -l <xxxk>: socket buffer
# -P <num>: number of clients in parallel
# -i <num>: interval summaries in seconds
# -t <num>: total duration of the test in secs
iperf -c 10.2.203.2 -l 128k -P 2 -i 1 -t 60
```

## nping

Ping on a variety of protocols

```sh
# ping on TCP on server listening at server.lan port 443, run the test 2 times
nping -c --tcp 443 server.lan

```

## `tc`

Traffic control utility.

```sh
# show qdisc config
tc qdisc show dev eth0

# change qdisc
tc qdisc add dev eth0 root <qdisc> <options>

# show qdisc stats
tc -s qdisc show dev eth0

# remove qdisc
tc qdisc del dev eth0 root
```

Example of using netem (network emulator) qdics for simulation

```sh
# show existing qdics on eth0
tc qdisc show dev eth0

# simulate packet loss of 1%
tc qdisc add dev eth0 root netem loss 1%
tc qdisc show dev eth0

# show summary
tc -s qdisc show dev eth0

# remove qdisc
tc qdisc del dev eth0 root netem
tc qdisc show dev eth0
```

See `tc-netem(8)` for more options.

Example of RTT two ways, specific to a port:
```sh
# Introducing delay of 74ms one end delay (~148ms RTT) to google

# Egress traffic
tc qdisc add dev eth0 root handle 1: prio priomap 0 0 0 0 2 2 2 2 0 0 0 0 2 2 2 2
tc qdisc add dev eth0 parent 1:2 handle 2: netem delay 74ms
tc filter add dev eth0 protocol ip prio 1 u32 match ip dport 443 0xffff match ip dst 216.239.38.120/32 flowid 1:2

# Ingress traffic, create pseudo interface so you can control it as egress traffic (into eth0)

## create ifb0 and attach netem to traffic from 10.10.10.2, which it would be egress traffic for ifb0
modprobe ifb
ip link set dev ifb0 up
tc qdisc add dev eth0 ingress
tc filter add dev eth0 parent ffff: protocol ip u32 match u32 0 0 flowid 1:1 action mirred egress redirect dev ifb0

tc qdisc add dev ifb0 root handle 1: prio priomap 0 0 0 0 2 2 2 2 0 0 0 0 2 2 2 2
tc qdisc add dev ifb0 parent 1:2 handle 2: netem delay 74ms
tc filter add dev ifb0 protocol ip prio 1 u32 match ip sport 443 0xffff match ip src 216.239.38.120/32 flowid 1:2

# test
nping --tcp-connect -p 443 216.239.38.120

# tear down
tc qdisc del dev eth0 root handle 1:
tc qdisc del dev ifb0 root handle 1:
tc qdisc del dev eth0 ingress
ip link set dev ifb0 down
modprobe -r ifb
```


# Tuning

```sh
sysctl -a | grep tcp
```


See also project TuneD.

## Socket Options

For `setsockopt(2)` syscall, in your program.

* `SO_SNDBUF`, `SO_RCVBUF`: rx/tx buffer sizes, which can be tuned up to the system limits.
* `SO_REUSEPORT`: allow multiple processes or threads to reuse port, having the kernel distribute load across them.
* `SO_MAX_PACING_RATE`: maximum pacing rates in B/s
* `SO_LINGER`: to reduce `TIME_WAIT` latency
* `TCP_NODELAY`: Disables Nagle, sending seg as soon as possible. This improves latency at the cost of higher network utilization
* `TCP_CORK`: Pause transmission until full packets can be sent, improving throughput. (see also `net.ipv4.tcp_autocorking`).
* `TCP_QUICKACK`: send ACKs immediately
* `TCP_CONGESTION`: congestion control alg for the socket

## Misc Tuning

Change MSS sizes (e.g. 1340) for a specific destination:

```sh
ip route add [dest ip range] via [gateway] dev eth0 advmss 1340
```

Change tcp retrans timeout minimum (rto) (e.g, 5ms) for a specific destination:

```sh
ip route add [dest ip range] via [gateway] dev eth0 rto_min 5ms
```