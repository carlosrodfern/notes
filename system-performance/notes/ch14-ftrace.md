# For older kernels

Examples

```sh
cd /sys/kernel/debug/tracing/
echo 'p:custom_d_lookup d_lookup' >> kprobe_events
echo 1 > events/kprobes/custom_d_lookup/enable
cat trace_pipe > /root/trace_output.txt
echo 0 > events/kprobes/custom_d_lookup/enable
echo '-:custom_d_lookup' >> kprobe_events
```

```sh
cd /sys/kernel/debug/tracing/
# first dereference for the struct ptr and +0 offset for the first field, second dereference for the char ptr.
echo 'p:custom_execve do_execve filename=+0(+0(%di)):string' >> kprobe_events
echo 1 > events/kprobes/custom_execve/enable
cat trace_pipe > /root/trace_output.txt
echo 0 > events/kprobes/custom_execve/enable
echo '-:custom_execve' >> kprobe_events
```

* System call args to registers: `man syscall 2`
* `+0(x)`: dereference pointer and offset `+0` (for structs).
