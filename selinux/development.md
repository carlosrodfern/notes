# Install sepolicy

```sh
sudo dnf install policycoreutils-devel
```

# Build example systemd internet services selinux


```sh
sepolicy generate --inetd /usr/bin/prometheus-pushgateway -w '/var/lib/prometheus-pushgateway(/.*)?'
```

# Utils

```sh
macro-expander 'inetd_service_domain(prometheus_pushgateway_t, prometheus_pushgateway_exec_t)'
```

Interfaces

```sh
sepolicy interface -l
```

selinux search
```sh
sesearch
```

Interface definition

```sh
grep -rn /usr/share/selinux/devel/include -e 'virt_kill'
```

# Troubleshooting

```sh
ausearch -m AVC
```