# How to download `rpm` from a task in koji

```
$ cat /etc/koji.conf.d/stream.conf

[stream]

;configuration for koji cli tool

;url of XMLRPC server
;server = http://hub.example.com/kojihub
server = https://kojihub.stream.centos.org/kojihub

;url of web interface
;weburl = http://www.example.com/koji
weburl = https://kojihub.stream.centos.org/koji

;url of package download site
;pkgurl = http://www.example.com/packages
topurl = https://kojihub.stream.centos.org

;path to the koji top directory
topdir = /kojifiles/

;configuration for Kerberos authentication

;the principal to auth as for automated clients
;principal = client@EXAMPLE.COM

;the keytab to auth as for automated clients
;keytab = /etc/krb5.keytab

; fedora uses kerberos auth
authtype = kerberos

;configuration for SSL authentication

;client certificate
;cert = ~/.koji/client.crt

;certificate of the CA that issued the HTTP server certificate
;serverca = ~/.koji/serverca.crt

;plugin paths, separated by ':' as the same as the shell's PATH
;koji_cli_plugins module and ~/.koji/plugins are always loaded in advance,
;and then be overridden by this option
;plugin_paths = ~/.koji/plugins

;[not_implemented_yet]
;enabled plugins for CLI, runroot and save_failed_tree are available
;plugins =
; runroot plugin is enabled by default in fedora
plugins = runroot

;timeout of XMLRPC requests by seconds, default: 60 * 60 * 12 = 43200
;timeout = 43200

;timeout of GSSAPI/SSL authentication by seconds, default: 60
;auth_timeout = 60

;enforcing CLI authentication even for anonymous calls
;force_auth = False

; use the fast upload feature of koji by default
use_fast_upload = yes

```

```sh
koji -p stream download-task [task id] --arch=x86_64 --skip=[skip, e.g. debug]
```

# Lifecycle of a change

The `.packit.yaml` in the github repo defines how to build the tar ball and the srpms, which then it is built in copr triggered by packit.dev (a build per PR). TMT can be defined here and packit.dev would run them. This is the upstream.

Then, the update ends up in Fedora/CentOS dist-git (rpms/mock and rpms/mock-core-configs), the downstream.
Them, zuul ci picks it up (since it is in rpms/* in the case of fedora) and runs the ci, which are ansible playbooks common to all, and one of them, `rpm-tmt-test`, runs found tmt tests in the rpms/_ repo itself. This involves builds on koji. If it passes, it is merged and built again in koji. Then, after that, I guess it goes the compose, more CI (like `t_functional`) and then pushing to mirrors
