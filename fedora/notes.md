# abi diff

Example:

```sh
abipkgdiff --d1 libcap-debuginfo-2.48-7.fc39.x86_64.rpm --d2 libcap-debuginfo-2.69-1.fc40.x86_64.rpm --devel1 libcap-devel-2.48-7.fc39.x86_64.rpm --devel2 libcap-devel-2.69-1.fc40.x86_64.rpm libcap-2.48-7.fc39.x86_64.rpm libcap-2.69-1.fc40.x86_64.rpm
```
